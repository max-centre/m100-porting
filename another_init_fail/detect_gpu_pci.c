#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <mpi.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
 
int main(int argc, char *argv[])
{
	int lSize;
	int lRank;
	int i = 0;
	int j = 0;
	int lNodeNameLength;
	int lNumRanksThisNode = 0;
	int lNumDevicesThisNode;
	int lRankThisNode;
	int lSizeThisNode;
	char lNodeName[MPI_MAX_PROCESSOR_NAME];
	char *lNodeNameRbuf;
	int *lRanksThisNode;
 
	MPI_Group lWorldGroup;
	MPI_Group lThisNodeGroup;
	MPI_Comm  lThisNodeComm;
   
	struct cudaDeviceProp deviceProp;
 
	MPI_Init(&argc,&argv);
 
	MPI_Comm_rank(MPI_COMM_WORLD, &lRank);
	MPI_Comm_size(MPI_COMM_WORLD, &lSize);
 
#if defined (__READ_ENV)
 
	const char* tmp = getenv("LOCAL_RANK");
	lRankThisNode = (int) atoi(tmp); 
 
#else
 
        MPI_Get_processor_name(lNodeName, &lNodeNameLength);
 
	lNodeNameRbuf = (char*) malloc(lSize * MPI_MAX_PROCESSOR_NAME * sizeof(char));
 
	MPI_Allgather(lNodeName, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, lNodeNameRbuf, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
 
	lRanksThisNode = (int*) malloc(lSize * sizeof(int));
	for(i=0; i<lSize; i++)
	{
		if(strncmp(lNodeName, (lNodeNameRbuf + i * MPI_MAX_PROCESSOR_NAME), MPI_MAX_PROCESSOR_NAME) == 0)
		{
			lRanksThisNode[lNumRanksThisNode] = i;
			lNumRanksThisNode++;
		}
	}
 
	MPI_Comm_group(MPI_COMM_WORLD, &lWorldGroup);
	MPI_Group_incl(lWorldGroup, lNumRanksThisNode, lRanksThisNode, &lThisNodeGroup);
	MPI_Comm_create(MPI_COMM_WORLD, lThisNodeGroup, &lThisNodeComm);
	MPI_Comm_rank(lThisNodeComm, &lRankThisNode);
	MPI_Comm_size(lThisNodeComm, &lSizeThisNode);
#endif
 
	// lRank = global MPI rank
	// lRankThisNode = local MPI rank within a node
 
	 i = cudaGetDeviceCount(&lNumDevicesThisNode);

                if ( i != cudaSuccess) {
                        printf("*** ERROR *** cudaGetDeviceCount(%d) failed!", i ); fflush(stdout);
                        exit(EXIT_FAILURE);
                }

	// lNumDevicesThisNode = number of GPU seen by a specific MPI
 
#if defined(__ORDERED)
        for(j = 0; j < lSize; j++) {
                MPI_Barrier(MPI_COMM_WORLD);
                if (j == lRank) {
#endif
	
	for (i = 0; i < lNumDevicesThisNode; i++) {
 
		if ( cudaSetDevice(i) != cudaSuccess) {
			printf("*** ERROR *** cudaSetDevice(%d) failed!", i ); fflush(stdout);
			exit(EXIT_FAILURE);
		}
 
		if ( cudaGetDeviceProperties(&deviceProp, i) != cudaSuccess) {
			printf("*** ERROR *** cudaGetDeviceProperties(%d) failed!", i ); fflush(stdout);
			exit(EXIT_FAILURE);
		}
 
		// Output
		printf("MPI %d (local rank %d), GPU ID : %d, pciBusID : %d, pciDeviceID : %d, pciDomainID : %d\n", 
			lRank, lRankThisNode, i, (int)deviceProp.pciBusID, (int)deviceProp.pciDeviceID, (int)deviceProp.pciDomainID);
	}
 
#if defined(__ORDERED)
		}
	}
#endif
 
#if !defined(__READ_ENV)
	MPI_Comm_free(&lThisNodeComm);
	MPI_Group_free(&lThisNodeGroup);
	MPI_Group_free(&lWorldGroup);
#endif
 
	MPI_Finalize();    
 
	return 0;
}
