program prova
   use cudafor
   use mpi
   implicit none
   integer, allocatable, device :: pippo(:)
   integer, allocatable         :: pluto(:)
   integer                      :: size, rank, ierror
   call MPI_INIT(ierror)
   call MPI_COMM_RANK (MPI_COMM_WORLD, rank, ierror)
   size= 100
   allocate(pippo(size),stat=ierror)
   if (ierror /=0) print *, 'allocation device failed: ', rank/4+1, mod(rank,4)+1, ierror
   allocate(pluto(size),stat=ierror)
   if (ierror /=0) print *, 'allocation host failed: ',   rank/4+1, mod(rank,4)+1, ierror
   if (rank==0) print *, 'ciao'
   call MPI_FINALIZE(ierror)
end program
