## MPI tasks and GPU binding

From the first run of Yambo we noticed that the speed-up expected by the usage fo the GPUs was not satisfied.
And out first suspect was a not correct binding between MPI tasks and GPUs. 
The suspicion was confirmed by an easy analysis of the run with the nvidia-smi tool. Allt the MPI tasks of a node were using the same GPU.
Checking online we found tha it was possible to bind the MPI tasks and the GPUs using an environment variable:

```
export CUDA_VISIBLE_DEVICES=$OMPI_COMM_WORLD_LOCAL_RANK
```

In order to properli use it was necessary to create a wrapper script containing the export and launch the executable in this way:

```
mpirun -np ${SLURM_NTASKS} ./gpu_bind.sh executable
```

## MPI tasks and threads binding

We launched our jobs using 4 MPI tasks per node, 32 threads per task (to exploit hyperthreading) and 4 GPUs per node (for a 1:1 binding with tasks):

```
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=32
#SBATCH --gres=gpu:4
```

```--ntasks-per-socket``` it is necessary for a correct balance of the tasks on the sockets.

In order to have a correct binding between MPI tasks and threads it is necessary to use two mpirun flags (cit. Isabella):

- ```--map-by socket:PE=X``` with X equal to the number of physical cores in the socket "object" 
due to the parallel element (PE) to map (with 4 MPI processes per node you have 8 physical cores per process);
- ```--rank-by core``` to avoid placing consecutive MPI ranks on different sockets

With this binding it was possible to obtain better results:

| # Nodes | 20 | 40 | 80 | 120 | 160 | 200 | 240 | 280 |
| -- | -- | -- | -- | -- | -- | -- | -- | -- |
| Threads binding | 2198 | 1175 | 558 | 438 | 315 | 349 | 232 | 234 |
| NO Threads binding | 1796 | 1025 | 510 | 371 | 304 | 285 | 215 | |

Here the mpirun line used in the final benchmarks:

```
mpirun --map-by socket:PE=8 --rank-by core \
       -np ${SLURM_NTASKS} ./gpu_bind.sh \
       ${YAMBO_BIN}/yambo -F yambo_${J_LABEL}.in -J ${J_LABEL}nodes.out

```

A complete job script is availale in this directory.
